;;; polymode-enhancements.el --- rmarkdown enhancements in polymode  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Shripad Sinari

;; Author: Shripad Sinari <shripad.sinari@gmail.com>
;; Keywords: maint, tools
;; Created: March 10, 2020
;; Version: 0.01

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; These are functions and tools that enhance the interaction in an
;; rmarkdown document with polymode as the major mode.
;; Provides navigation and folding capabilities similar to org-mode.
;; also adds org-ref style bibtex entry insertion.
;; Additionally, often one needs to package only the bibtex entries that have been
;; referred to in the document. Function to create such a bibliography file is provided.

;;; Code:

(require 'ess)
(require 'dash)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; rendering and run functions for the rmarkdown document based on the output

;;;###autoload
(defun rmd-render ()
  "render R markdown (.Rmd). Should work for any output type."
  (interactive)
  ;; Check if attached R-session
  (condition-case nil
      (ess-get-process)
    (error 
     (ess-switch-process)))
  (let* ((rmd-buf (current-buffer)))
    (save-excursion
      (let* ((sprocess (ess-get-process ess-current-process-name))
	     (sbuffer (process-buffer sprocess))
	     (buf-coding (symbol-name buffer-file-coding-system))
	     (R-cmd
	      (format "library(rmarkdown); rmarkdown::render(\"%s\", output_format = \"all\")"
		      buffer-file-name)))
	(message "Running rmarkdown on %s" buffer-file-name)
	(ess-execute R-cmd 'buffer nil nil)))))

;;;###autoload
(defun rmd-run-shiny ()
  "run R markdown (.Rmd) as a shiny app."
  (interactive)
  ;; Check if attached R-session
  (condition-case nil
      (ess-get-process)
    (error 
     (ess-switch-process)))
  (let* ((rmd-buf (current-buffer)))
    (save-excursion
      (let* ((sprocess (ess-get-process ess-current-process-name))
	     (sbuffer (process-buffer sprocess))
	     (buf-coding (symbol-name buffer-file-coding-system))
	     (R-cmd
	      (format "library(rmarkdown); rmarkdown::run(\"%s\")"
		      buffer-file-name)))
	(message "Running rmarkdown on %s" buffer-file-name)
	(ess-execute R-cmd 'buffer nil nil)))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; rmarkdown chunk related functions.
;; Involves
;; 1. navigation
;; 2. folding

(setq polymode-chunk-start-end-regexp-alist
      '((ess . ("^```{r.*}" "^```$"))))

(defun polymode-chunk-start (host-mode)
  (car (alist-get host-mode polymode-chunk-start-end-regexp-alist)))

(defun polymode-chunk-end (host-mode)
  (cadr (alist-get host-mode polymode-chunk-start-end-regexp-alist)))

(polymode-chunk-start 'ess)
(polymode-chunk-end 'ess)

(defun rmd-split-chunk-at-point ()
  "Split the r code chunk at point."
  (interactive)
  (setq rmd-code-chunk-start
   (progn
     (setq start (string-match "^" (polymode-chunk-start 'ess)) )
     (setq end  (+ (string-match "r" (polymode-chunk-start 'ess)) 1))
     (substring (polymode-chunk-start 'ess) start end)))
  (setq rmd-code-chunk-options-end
   (progn
     (setq end  (length (polymode-chunk-start 'ess)))
     (substring (polymode-chunk-start 'ess) (- end 1) end)))
  (setq rmd-code-chunk-end
   (progn
     (setq start (+ (string-match "^" (polymode-chunk-end 'ess)) 1))
     (setq end (- (string-match "$" (polymode-chunk-end 'ess)) 1))
     (substring (polymode-chunk-end 'ess) start end)))
  (save-excursion
    (save-match-data
      (widen)
      (move-end-of-line 1)
      (progn
	(setq start (search-backward-regexp rmd-code-chunk-start nil nil))
	(setq end (search-forward-regexp rmd-code-chunk-options-end nil nil))
	(copy-region-as-kill start end))))
  (newline)
  (beginning-of-line)
  (insert rmd-code-chunk-end)
  (newline 2)
  (yank)
  (newline))

(defun rmd-next-code-chunk ()
  (interactive)
  (widen)
  (goto-char (+ (search-forward-regexp (polymode-chunk-start 'ess) nil t) 1)))

(defun rmd-previous-code-chunk ()
  (interactive)
  (widen)
  (goto-char (search-backward (polymode-chunk-end 'ess)))
  (goto-char (search-backward-regexp (polymode-chunk-start 'ess) nil t))
  (goto-char (+ (search-forward "}") 1)
	     ))

(defun rmd-fold-chunk-at-point (pos)
  "Fold the r code chunk at point (as POS)."
  (interactive)
  (save-excursion
    (save-match-data
      (widen)
      (move-end-of-line 1)
      (search-backward-regexp "^```{r[^,}]*")
      (setq start (- (search-forward-regexp "[,}]" nil t) 1))
      (setq end (search-forward-regexp (polymode-chunk-end 'ess)))
      (if (< pos end) ;; if initial position is within a code chunk do folding.
	  (fold-this start end)))))


(defun fold-r-chunks-excluding-start ()
  "Fold R code chunks excluding the chunk start line."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "^```{r" nil t)
      (let ((start (match-end 0))  ; Adjust to end of the chunk start line
            (end nil)
            (fold-end nil))
        ;; Find the end of the current chunk
        (if (re-search-forward "^```" nil t)
            (setq end (match-end 0)))
        ;; Check for consecutive chunks separated by blank lines
        (while (and end
                    (looking-at "[[:space:]]*\n```{r")
                    (re-search-forward "^```{r" nil t))
          (if (re-search-forward "^```" nil t)
              (setq fold-end (match-end 0))))
        ;; Apply folding
        (if fold-end
            (fold-this start fold-end)
          (when end
            (fold-this start end)))
        (goto-char (or fold-end end (point-max)))))))


(defun fold-r-chunks-and-toggle-markup-hiding ()
  "Fold R code chunks first, then toggle hiding of markdown markup."
  (interactive)
  ;; Fold R code chunks
  (fold-r-chunks-excluding-start)
  ;; Toggle markdown markup hiding
  (markdown-toggle-markup-hiding 1))


(defun toggle-unfold-and-markup-hiding ()
  "Toggle unfolding all R code chunks and hiding of markdown markup."
  (interactive)
  ;; Unfold all folded regions
  (fold-this-unfold-all)

  ;; Toggle markdown markup hiding
  (markdown-toggle-markup-hiding -1))

(defvar toggle-state-folding-hiding nil "Tracks the toggle state between folding/hiding and unfolding/unhiding.")

(defun toggle-folding-and-markup ()
  "Toggle between folding/hiding and unfolding/unhiding states."
  (interactive)
  (if toggle-state-folding-hiding
      (progn
        (toggle-unfold-and-markup-hiding)
        (setq toggle-state-folding-hiding nil))
    (progn
      (fold-r-chunks-and-toggle-markup-hiding)
      (setq toggle-state-folding-hiding t))))

;;;###autoload
(defun rmd-fold-chunk ()
  "Fold the current chunk at point."
  (interactive)
  (rmd-fold-chunk-at-point (point)))

;;;###autoload
(defun rmd-fold-all-chunks ()
  "Fold all r code chunks in current buffer."
  (interactive)
  (save-excursion
    (save-match-data
      (widen)
      (goto-char (point-min))
      (while  (search-forward-regexp (polymode-chunk-start 'ess) nil t)
	(rmd-fold-chunk)))))

;;;###autoload
(defun rmd-unfold-chunk ()
  "Unfold the current r code chunk."
  (interactive)
  (save-excursion
    (save-match-data
      (widen)
      (move-beginning-of-line 1)
      (goto-char (next-overlay-change (point)))
      (fold-this-unfold-at-point))))

;;;###autoload
(defun rmd-unfold-all-chunks ()
  "Fold all r code chunks in current buffer."
  (interactive)
  (save-excursion
    (save-match-data
      (widen)
      (goto-char (point-min))
      (while  (search-forward-regexp (polymode-chunk-start 'ess) nil t)
	(rmd-unfold-chunk)))))

;;;###autoload (autoload 'toggle-function "polymode-enhancements")
(defmacro toggle-function (name &rest fns)
  "Toggle function with NAME. Toggle between two functions FNS."
  ;; use a property “folded”. Value is t or nil
  `(defun ,name ()
     ;;(concat "Toggle between functions %s and %s" ,@fns)
     (interactive)
     (fset 'folding-fn (nth 0 ,@fns))
     (fset 'unfolding-fn (nth 1 ,@fns))
     (if (get ',name 'folded)
	 (progn
	   (unfolding-fn)
	   (put ',name 'folded nil))
       (progn
	 (folding-fn)
	 (put ',name 'folded t)))))

;;;###autoload
(defun rmd-toggle-folding-all ()
  "Toggle folding of all code chunks."
  (interactive)
  (if (get 'rmd-toggle-folding-all 'folded)
      (progn
	(rmd-unfold-all-chunks)
	(put 'rmd-toggle-folding-all 'folded nil))
    (progn
      (rmd-fold-all-chunks)
      (put 'rmd-toggle-folding-all 'folded t))))

;;;###autoload
(defun rmd-toggle-fold-chunk ()
  "Toggle folding of all code chunks."
  (interactive)
  (if (get 'rmd-toggle-fold-chunk 'chunk-folded)
      (progn
	(rmd-unfold-chunk)
	(put 'rmd-toggle-fold-chunk 'chunk-folded nil))
    (progn
      (rmd-fold-chunk)
      (put 'rmd-toggle-fold-chunk'chunk-folded t))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ESS mode related functions in an rmarkdown document.
;; Involves
;; 1. folding

(defun ess-fold-function-at-point (pos)
  "Fold the r function at point (POS)."
  (interactive)
  (save-excursion
    (save-match-data
      (widen)
      ;; seems to be the only way to get function block
      ;; information, i.e. start and end of the function.
      (move-end-of-line 1)
       (ess-goto-beginning-of-function-or-para) 
      (setq start (search-forward-regexp ")"))
      (ess-goto-end-of-function-or-para)
      (setq end (+ 1 (search-backward-regexp ".")))
      (let ((fold-this-overlay-text "{...}"))
	(if (< pos end) ;; if initial position is within a function do folding.
	    (fold-this start end))))))

;;;###autoload
(defun ess-fold-function ()
  "Fold the function at point."
  (interactive)
  (ess-fold-function-at-point (point)))

;;;###autoload
(defalias 'ess-unfold-function 'rmd-unfold-chunk)

(defun get-chunk-start ()
    "Get the start position of the chunk."
  (interactive)
  (save-excursion
    (save-match-data
      (widen)
      (move-end-of-line 1)
      (search-backward-regexp "^```{r[^,}]*")
      (match-beginning 0))))

(defun get-chunk-end ()
  "Get the end position of the chunk."
  (interactive)
  (save-excursion
    (save-match-data
      (widen)
      (move-end-of-line 1)
      (search-forward-regexp "^```$")
      (match-beginning 0))))

;;;###autoload
(defun ess-fold-functions ()
  "Fold all r functions in current buffer."
  (interactive)
  (save-excursion
    (save-match-data
      (goto-char (get-chunk-start))
      (while  (search-forward-regexp "<-[[:space:]]+function(.*?)" (get-chunk-end) t)
	(ess-fold-function)))))

;;;###autoload
(defun ess-unfold-functions ()
  "Fold all r functions in current buffer."
  (interactive)
  (save-excursion
    (save-match-data
      (goto-char (get-chunk-start))
      (while  (search-forward-regexp "<-[[:space:]]+function(.*?)" (get-chunk-end) t)
	(fold-this-unfold-at-point)))))

;;;###autoload
(defun ess-fold-all-functions ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (rmd-next-code-chunk)
      (ess-fold-functions))))

;;;###autoload
(defun ess-unfold-all-functions ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (rmd-next-code-chunk)
      (ess-unfold-functions))))

;;;###autoload
(defun ess-toggle-folding-all-functions ()
  "Toggle folding of all functions."
  (interactive)
  (if (get 'ess-toggle-folding-all-functions 'all-fns-folded)
      (progn
	((ess-unfold-all-functions)
	 put 'ess-toggle-folding-all-functions 'all-fns-folded nil))
    (progn
      (ess-fold-all-functions)
      (put 'ess-toggle-folding-all-functions 'all-fns-folded t))))

;;;###autoload
(defun ess-toggle-fold-functions ()
  "Toggle folding of all functions chunks."
  (interactive)
  (if (get 'ess-toggle-fold-functions 'all-fns-folded)
      (progn
	(ess-unfold-functions)
	(put 'ess-toggle-fold-functions 'all-fns-folded nil))
    (progn
      (ess-fold-functions)
      (put 'ess-toggle-fold-functions 'all-fns-folded t))))

(defun see-r-chunks ()
  "Get the header of all R code chunks in the buffer."
  (interactive)
  (occur "^```{r"))

;;;###autoload
(defun then_R_operator ()
  "R - %>% operator or 'then' pipe operator."
  (interactive)
  (just-one-space 1)
  (insert "%>%")
  (reindent-then-newline-and-indent))

;;;###autoload
(defun base_R_pipe ()
  "R - |> operator or 'base R' pipe operator."
  (interactive)
  (just-one-space 1)
  (insert "|>")
  (reindent-then-newline-and-indent))

;;;###autoload
(defun base_R_pipe_inline ()
  "R - |> operator or 'base R' pipe operator."
  (interactive)
  (just-one-space 1)
  (insert "|>")
  (just-one-space 1))

;;;###autoload
(defun plus_GG_operator ()
  "GGplot - + operator."
  (interactive)
  (just-one-space 1)
  (insert "+")
  (reindent-then-newline-and-indent))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; functions to introduce bibtex related manipulation in rmarkdown document.

;;----------------------------------------------------------------------------------------------------;;
;; Borrowed from:
;; https://emacs.stackexchange.com/questions/7148/get-all-regexp-matches-in-buffer-as-a-list
;; (defun re-seq (regexp string)
;;   "Get a list of all regexp matches in a string"
;;   (save-match-data
;;     (let ((pos 0)
;;           matches)
;;       (while (string-match regexp string pos)
;;         (push (match-string 0 string) matches)
;;         (setq pos (match-end 0)))
;;       matches)))

;; ; Sample URL
;; (setq urlreg "\\(?:http://\\)?www\\(?:[./#\+-]\\w*\\)+")
;; ; Sample invocation
;; (re-seq urlreg (buffer-string))
;;----------------------------------------------------------------------------------------------------;;


(defun re-uniq-seq (regexp string)
  "Get a list of unique regexp (REGEXP) match in a string (STRING)."
  (save-match-data
    (let ((pos 0)
	  matches)
      (while (string-match regexp string pos)
	(push (match-string-no-properties 1 string) matches)
	(setq pos (match-end 1)))
      (-uniq matches))))

(setq bibtex-entry-format-alist
      ;; This regexp works better than "\\[.*?@\\(.+\\)\\]"
      ;; Since it excludes any email address due to presence of the
      ;; closing ]. Now the regex has delimiters that separate
      ;; bibtex entries.
      '((markdown-mode . "@\\([^];,\\.]+\\).*?\\]")))

(defun get-all-bibtex-entries (buffer)
  (with-current-buffer buffer
    (re-uniq-seq (cdr (assoc major-mode bibtex-entry-format-alist)) (buffer-string))))

;; (defun insert-entry (entry)
;;   (insert (bibtex-completion-make-bibtex entry)))
;; original bibtex-completion-make-bibtex puts one more pair of {}
;; around each key value pair. not needed for well-formed entry.
(defun ss-bibtex-completion-make-bibtex (key)
  (let* ((entry (bibtex-completion-get-entry key))
	 (entry-type (bibtex-completion-get-value "=type=" entry)))
    (format "@%s{%s,\n%s}\n"
	    entry-type key
	    (cl-loop
	     for field in entry
	     for name = (car field)
	     for value = (cdr field)
	     unless (member name
			    (append (-map (lambda (it) (if (symbolp it) (symbol-name it) it))
					  bibtex-completion-no-export-fields)
				    '("=type=" "=key=" "=has-pdf=" "=has-note=" "crossref")))
	     concat
	     (if (string-match-p "^{.*}$" value)
		 (format "  %s = %s,\n" name value)
	       (format "  %s = {%s},\n" name value))))))

(defun insert-entry (entry)
  (insert (ss-bibtex-completion-make-bibtex entry)))

(defun get-bibliography-filename (buffer)
  (with-current-buffer buffer
    (if (string-match "^bibliography:[[:space:]]+\\([^[:space:]]+\\).*$" (buffer-string))
	(match-string-no-properties 1 (buffer-string))
      nil)))

;;;###autoload
(defun create-bib ()
  (interactive)
  (let* ((source-buffer (buffer-name))
	 (bib-file (get-bibliography-filename source-buffer))
	 (bibtex-entries (get-all-bibtex-entries (buffer-name))))
    (if bib-file
	(progn
	  (switch-to-buffer (generate-new-buffer bib-file))
	  (dolist (entry bibtex-entries) (insert-entry entry))
	  (write-file bib-file)
	  (kill-buffer bib-file)
	  (switch-to-buffer source-buffer))
      nil)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'polymode-enhancements)
;;; polymode-enhancements.el ends here

